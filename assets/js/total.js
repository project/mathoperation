(function ($, Drupal, drupalSettings) {
'use strict';
  Drupal.behaviors.mathoperation = {
    attach: function (context, settings) {
      //disable mouse right click
      $(".mathoperation_op1").on("contextmenu", function () {
        return false;
      });
      $(".mathoperation_op2").on("contextmenu", function () {
        return false;
      });
      //event listener on inputs and magnitude
      $(".mathoperation_op1, .mathoperation_op2").on("keyup", calc);
      $("select.mathoperation_magnitude").on("change", calc);
      // caluclation of two inputs with operator and then magnitude the total
      function calc() {
        let operand1 = parseFloat($('.mathoperation_op1').val()) || 0;
        let operand2 = parseFloat($('.mathoperation_op2').val()) || 0;
        let mathoperator = drupalSettings.mathoperation.operator;
        let magnitude = $("select.mathoperation_magnitude option:selected").val();
        let total = window.eval(operand1 + mathoperator + operand2);
        if (isNaN(magnitude) === false)
        total = parseFloat(total) * parseFloat(magnitude);
         $('.mathoperation_total').val(total);
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
