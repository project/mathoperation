<?php

namespace Drupal\mathoperation\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'math_operation_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "math_operation_formatter",
 *   label = @Translation("Arithmetic Operation"),
 *   field_types = {
 *     "math_operation_type"
 *   }
 * )
 */
class MathOperationFormatter extends FormatterBase
{

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings()
  {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state)
  {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary()
  {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode)
  {
    $elements = [];

    foreach ($items as $delta => $item) {
      // Do not print empty headings.
      if (empty($item->total)) {
        continue;
      }
      $elements[$delta] = [
        '#theme' => 'mathoperation',
        '#total_prefix' => $this->getFieldSetting('prefix'),
        '#total_suffix' => $this->getFieldSetting('suffix'),
        '#operand1' => $item->operand1,
        '#operand2' => $item->operand2,
        '#magnitude' => $item->magnitude,
        '#total' => $item->total,
      ];
    }

    return $elements;
  }

}
