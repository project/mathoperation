<?php

namespace Drupal\mathoperation\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'math_operation_type' field type.
 *
 * @FieldType(
 *   id = "math_operation_type",
 *   label = @Translation("Arithmetic Operation"),
 *   description = @Translation("Arithmetic operation between the two operand or one input operand with predefined operand"),
 *   default_widget = "math_operation_widget",
 *   default_formatter = "math_operation_formatter"
 * )
 */
class MathOperationType extends FieldItemBase {

 /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings = [
      'prefix' => '',
      'suffix' => '',
      'operator' => '',
      'magnitude' => '1|One' . PHP_EOL .
        '10|Ten (da)' . PHP_EOL .
        '100|Hundred (h)' . PHP_EOL .
        '1000|Thousand (k)' . PHP_EOL .
        '1000000|Million (M)' . PHP_EOL .
        '1000000000|Billion (G)' . PHP_EOL .
        '1000000000000|Trillion (T)',
    ];
    return $settings + parent::defaultFieldSettings();
  }
  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['operand1'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Operand 1'));
      $properties['operand2'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Operand 2'));
      $properties['total'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Total'));
      $properties['magnitude'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Magnitude'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $schema = [
      'columns' => [
        'operand1' => [
          'type' => 'varchar',
          'length' => 256,
          'not null' => TRUE,
          'default' => '',
        ],
        'operand2' => [
          'type' => 'varchar',
          'length' => 256,
          'not null' => TRUE,
          'default' => '',
        ],
        'magnitude' => [
          'type' => 'varchar',
          'length' => 256,
          'not null' => TRUE,
          'default' => '',
        ],
        'total' => [
          'type' => 'varchar',
          'length' => 256,
          'not null' => TRUE,
          'default' => '',
        ],
      ],
    ];

    return $schema;
  }


  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);
    $allowed_settings = $this->getSetting('magnitude');
    $default_value = !empty($allowed_settings) ? $allowed_settings : '';

    $element['prefix'] = [
      '#type' => 'textfield',
      '#title' => t('Prefix'),
      '#default_value' => $this->getSetting('prefix'),
      '#description' => t('The prefix text displays on fields.'),
      '#size' => 10,
      '#maxlength' => 128,
    ];
    $element['suffix'] = [
      '#type' => 'textfield',
      '#title' => t('Suffix'),
      '#default_value' => $this->getSetting('suffix'),
      '#description' => t('The suffix text displays on fields.'),
      '#size' => 10,
      '#maxlength' => 128,
    ];
    $element['operator'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('Select Operator'),
      '#default_value' => $this->getSetting('operator'),
      '#options' => [
        '+' => $this->t('Add'),
        '-' => $this->t('Subtract'),
        '*' => $this->t('Multiply'),
        '/' => $this->t('Divide'),
      ],
    ];
    $element['magnitude'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Enter the magnitude'),
      '#description' => '<p>' . t('The possible values. Enter one value per line, in the format key|label.'),
      '#attributes' => [
        'placeholder' => '1000|Thousand (k)' . PHP_EOL . '1000000|Million (M)',
      ],
      '#default_value' => $default_value,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $operand1 = $this->get('operand1')->getValue();
    $operand2 = $this->get('operand2')->getValue();
    $total = $this->get('total')->getValue();
    $magnitude = $this->get('magnitude')->getValue();
    return empty($operand1) && empty($operand2) && empty($total) && empty($magnitude);
  }

}
