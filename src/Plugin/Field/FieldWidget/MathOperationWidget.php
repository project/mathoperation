<?php

namespace Drupal\mathoperation\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'math_operation_widget' widget.
 *
 * @FieldWidget(
 *   id = "math_operation_widget",
 *   module = "mathoperation",
 *   label = @Translation("Arithmetic Operation"),
 *   field_types = {
 *     "math_operation_type"
 *   },
 *   multiple_values = FALSE,
 * )
 */
class MathOperationWidget extends WidgetBase
{

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings()
  {
    return [
      'size' => 60,
      'placeholder' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state)
  {
    $elements = [];

    $elements['size'] = [
      '#type' => 'number',
      '#title' => t('Size of textfield'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
      '#min' => 1,
    ];
    $elements['placeholder'] = [
      '#type' => 'textfield',
      '#title' => t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary()
  {
    $summary = [];

    $summary[] = t('Textfield size: @size', ['@size' => $this->getSetting('size')]);
    if (!empty($this->getSetting('placeholder'))) {
      $summary[] = t('Placeholder: @placeholder', ['@placeholder' => $this->getSetting('placeholder')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state)
  {
    $element['#attached']['library'][] = 'mathoperation/assets';
    $element['#attached']['drupalSettings']['mathoperation']['operator'] = $this->getFieldSetting('operator');

    $element['operand1'] = [
      '#type' => 'textfield',
      '#title' => $this
        ->t('Operand 1'),
      '#default_value' => isset($items[$delta]->operand1) ? $items[$delta]->operand1 : NULL,
      '#attributes' => ['class' => ['mathoperation_op1']],
      '#size' => $this->getSetting('size'),
      '#placeholder' => $this->getSetting('placeholder'),
      '#maxlength' => 255,
    ];
    $element['operand2'] = [
      '#type' => 'textfield',
      '#title' => $this
        ->t('Operand 2'),
      '#default_value' => isset($items[$delta]->operand2) ? $items[$delta]->operand2 : NULL,
      '#attributes' => ['class' => ['mathoperation_op2']],
      '#size' => $this->getSetting('size'),
      '#placeholder' => $this->getSetting('placeholder'),
      '#maxlength' => 255,
    ];
    $element['magnitude'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('Magnitude'),
      '#options' => $this->getSelectOptions($this->getFieldSetting('magnitude')),
      '#attributes' => ['class' => ['mathoperation_magnitude']],
      '#default_value' => isset($items[$delta]->magnitude) ? $items[$delta]->magnitude : NULL,
    ];
    $element['total'] =  [
      '#type' => 'textfield',
      '#title' => $this->t('Total'),
      '#default_value' => isset($items[$delta]->total) ? $items[$delta]->total : NULL,
      '#size' => $this->getSetting('size'),
      '#attributes' => ['class' => ['mathoperation_total'], 'readonly' => 'readonly'],
      '#placeholder' => $this->getSetting('placeholder'),
      '#maxlength' => 255,
    ];

    return $element;
  }

  /**
   * Convert textarea lines into an array.
   *
   * @param string $string
   *   The textarea lines to explode.
   * @param bool $summary
   *   A flag to return a formatted list of classes available.
   *
   * @return array
   *   An array keyed by the classes.
   */
  protected function getSelectOptions($string, $summary = FALSE)
  {
    $options = [];
    $lines = preg_split("/\\r\\n|\\r|\\n/", trim($string));
    $lines = array_filter($lines);

    foreach ($lines as $line) {
      list($class, $label) = explode('|', trim($line));
      $label = $label ?: $class;
      $options[$class] = $label;
    }

    if ($summary) {
      return implode(', ', array_keys($options));
    }

    return $options;
  }
}
